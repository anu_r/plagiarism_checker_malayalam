import unicodedata
from flask import Flask, render_template, flash, request, url_for
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from markupsafe import Markup
import os
import time

from arre import *

DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'

class ReusableForm(Form):
	name = TextField('Name:', validators=[validators.required()])

@app.route('/', methods=['GET', 'POST'])
def my_form():
	form = ReusableForm(request.form)
	print (form.errors)
    
	if request.method == 'POST':
		sentence=request.form['name']
        
	if form.validate():          
		plagiarismlist=pla(sentence)
		if plagiarismlist==[]:
			flash ("no plagiarized documents")
		else:
			for i in plagiarismlist:
				filename=i[0]
				cosine=i[1]
				desc=des(filename)
				er=(desc,"cosine similarity ", cosine)
				flash(er)
            
	return render_template('home.html', form=form)

if __name__ == '__main__':
	app.run()
